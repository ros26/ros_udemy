#include <ros/ros.h>
#include <std_msgs/Int64.h>

int count = 0;


void callback_number(const std_msgs::Int64& msg){
	count += msg.data;
	}

int main(int argc, char **argv){
	ros::init(argc, argv, "number_counter");
	ros::NodeHandle nh;

	ros::Subscriber sub = nh.subscribe("/number", 1000, callback_number);
	ros::Publisher pub = nh.advertise<std_msgs::Int64>("/number_count", 10);
	ros::spin();
	ros::Rate rate(10);
	
	while(ros::ok()){
		std_msgs::Int64 msg1;
		msg1.data = count;
		pub.publish(msg1);
		ros::spinOnce();
		rate.sleep();
		
		

		}
}
