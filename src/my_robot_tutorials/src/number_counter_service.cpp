#include <ros/ros.h>
#include <std_msgs/Int64.h>
#include <std_srvs/SetBool.h>
int count = 0;
ros::Publisher pub;

void callback_number(const std_msgs::Int64& msg)
{
	count += msg.data;
	std_msgs::Int64 new_msg;
	new_msg.data = count;
	pub.publish(new_msg);
}

bool callback_reset(std_srvs::SetBool::Request &req, std_srvs::SetBool::Response &res){
	if(req.data) {
		count = 0;
		res.success = true;
		res.message = "reset done";
	}
	else {
		res.success = false;
		res.message = "reset not done";
  	}
	return true;

}

int main (int argc, char **argv)
{
	ros::init(argc, argv, "number_counter");
	ros::NodeHandle nh;

	ros::Subscriber sub = nh.subscribe("/number", 1000, callback_number);

	pub = nh.advertise<std_msgs::Int64>("/number_count", 10);
	ros::ServiceServer server = nh.advertiseService("/reset_number_count", callback_reset);
	ros::spin();
}
